import os
import zipfile


def pack(root, rel_path, out_file, new_rel_path=None, ignore_dotfiles=False):
    with zipfile.ZipFile(out_file, 'w') as zf:
        for path, dirs, files in os.walk(os.path.join(root, rel_path), topdown=True):
            if ignore_dotfiles:
                dirs[:] = [d for d in dirs if not d.startswith(".")] 
            for file in files:
                rp = os.path.relpath(os.path.join(path, file), root)
                if new_rel_path is not None:
                    rp = os.path.relpath(rp, rel_path)
                    rp = os.path.join(new_rel_path, rp)
                zf.write(os.path.join(path, file), rp)
